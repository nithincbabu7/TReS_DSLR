# OMP_NUM_THREADS=1 python ./run.py  --num_encoder_layerst 2 --dim_feedforwardt 64 --nheadt 16 --network 'resnet50' --batch_size 53  --svpath  './results/'   --droplr 1 --epochs 3 --gpunum '0'  --datapath  '/home/ece/nithinc_datasets/DSLR/Dataset_in_one_folder/' --dataset 'dslr' --seed 2021 --vesion 1 --split 1 --test_code


for i in {1..100}
do
    echo "split $i"
    OMP_NUM_THREADS=1 python ./run.py  --num_encoder_layerst 2 --dim_feedforwardt 64 --nheadt 16 --network 'resnet50' --batch_size 53  --svpath  './results/'   --droplr 1 --epochs 5 --gpunum '0'  --datapath  '/home/ece/nithinc_datasets/DSLR/Dataset_in_one_folder/' --dataset 'dslr' --seed 2021 --vesion 1 --split $i
done
