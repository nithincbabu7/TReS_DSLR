import pandas as pd
import os
import statistics

def perf(version, max_splits):
    res_dir = './results/'+version+'/'
    dir_list = [x[0] for x in os.walk(res_dir)]
    dir_list = [x for x in dir_list if os.path.isfile(x+'/sv/res.csv')]
    if len(dir_list) > max_splits:
        dir_list = dir_list[:max_splits]
    srcc = []
    plcc = []
    for v_dir in dir_list:
        df = pd.read_csv(v_dir+'/sv/res.csv')
        srcc.append(df[df['Metric']=='SRCC']['Value'].values[0])
        plcc.append(df[df['Metric']=='PLCC']['Value'].values[0])
    
    print(srcc, statistics.mean(srcc), statistics.median(srcc))
    print(plcc, statistics.mean(plcc), statistics.median(plcc))
    df_res = pd.DataFrame({'Metric': ['SRCC', 'PLCC'],
                           'Mean': [statistics.mean(srcc), statistics.mean(plcc)],
                           'Median': [statistics.median(srcc), statistics.median(plcc)]})
    df_res.to_csv(res_dir + '/combined.csv')
    
perf('v1', 5)
