import sys
sys.path.insert(0, '/mnt/e4e2203b-ecf7-4807-b641-1fa71921092d/VIGNESH_benchmarking/maxentropy-master/')

import os
import pandas as pd
import numpy as np
import torch
import csv
from torch.autograd import Variable
from tqdm import tqdm
from glob import glob
from torchvision.transforms.functional import to_tensor  
import torch.optim as optim
from torch.optim.lr_scheduler import MultiStepLR
from torch.utils.data import Dataset, DataLoader
import time
import cv2
from get_labels import get_max_entropy_distribution
from emd_loss import earth_movers_distance as emd
from model_nima import nima_features, nima_fc
import torch.nn as nn

def Train_nima():

    ps = 512
    
    save_freq = 1
    cuda1 = torch.device('cuda:0')
    # save_dir ='./nima_checkpoints/'
    save_dir = '/media/ece/Backup Plus/Vignesh/nima_checkpoints/'
    img_dir = '/media/ece/Backup Plus/Vignesh/Dataset_in_one_folder/'
    # score_dir = ''
    
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)
    
    ## creating the dataset
    class SIDDataset(Dataset):
        def __init__(self, img_dir,MOS,img_names):
           
            self.paths_img = [img_dir + i for i in img_names]
            self.moss = MOS
            
        def __len__(self):
            # return size of dataset
            return len(self.paths_img)
        
        def read_data(self, path):
            
            img = cv2.imread(path)
            img  = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            img = np.moveaxis(img, (0,1,2), (1,2,0))
            
            img = img.astype(np.float32)
            img = img/255
            
            img[:1,:,:] = (img[0:1,:,:]-0.485)/0.229
            img[1:2,:,:] = (img[1:2,:,:]-0.456)/0.224
            img[2:3,:,:] = (img[2:3,:,:]-0.406)/0.225
            
            return img

        def extract_patches(self, img):
            
            H = img.shape[1]
            W = img.shape[2]
            
            xx = np.random.randint(0,W-ps)
            yy = np.random.randint(0,H-ps)
            patch = img[:,yy:yy+ps,xx:xx+ps]
                
            patch = torch.tensor(patch.copy(), device=cuda1, dtype=torch.float32)
            
            return patch
           
        def __getitem__(self, idx):
           
            path = self.paths_img[idx]
            name_img = os.path.basename(path)
            
            img = self.read_data(path)
            patch = self.extract_patches(img)
          
            df = self.moss
            mos = df['MOS'][df['image_name'] == name_img]
            mos = mos.values[0]
           
            dist = get_max_entropy_distribution(mos/10) 
            dist = torch.tensor(dist, device=cuda1, dtype=torch.float32)
           
            mos = torch.tensor(mos.copy(), device=cuda1, dtype=torch.float32) 
           
            return patch, dist, mos

    mosd = pd.read_csv(img_dir + 'MOS.csv',
                      names=['tp', 'image_name', 'MOS'])
    mosd.drop(mosd.columns[0], inplace=True, axis=1)
    mosd = mosd.drop([mosd.index[0]])
    
    for split in range(1, 101):
    
        print(split)
    
        with open(img_dir + 'trainsplits.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for count, row in enumerate(csv_reader):
                if count == split:
                    temp = row
    
        img_names = []
    
        for name in temp:
            if len(name) > 2:
                img_names.append(name)
    
        save_dir_mod = save_dir + 'split_' + str(split) + '/'
    
        if not os.path.exists(save_dir_mod):
            os.mkdir(save_dir_mod)   
        train_dataloader = DataLoader(SIDDataset(img_dir,mosd,img_names), 
                                      batch_size=16, shuffle=True)
        
        model_features = nima_features()
        model_features.train()
        model_features = model_features.cuda(cuda1)
        
        model_fc = nima_fc()
        model_fc.train()
        model_fc = model_fc.cuda(cuda1)
        
        opt_features = optim.Adam(model_features.parameters(), lr=3e-7, weight_decay=0)
        opt_fc = optim.Adam(model_fc.parameters(), lr=1e-3, weight_decay=0)
        scd_features = MultiStepLR(opt_features, milestones=[30], gamma=0.1)
        scd_fc = MultiStepLR(opt_fc, milestones=[10,20], gamma=0.1)
        
        for epoch in range(25):
            scd_fc.step(epoch)  # step to the learning rate in this epcoh
            
            epoch_loss = 0
            
            start_time = time.time()
            
            for n_count, batch_yx in enumerate(train_dataloader):
                
                opt_features.zero_grad()
                opt_fc.zero_grad()
                
                img_data, dist_gt, mos_gt = Variable(batch_yx[0]), Variable(batch_yx[1]), Variable(batch_yx[2])
                # mos_gt = mos_gt.unsqueeze(dim=1)
                
                features = model_features(img_data)
                                
                dist_pred = model_fc(features)
                # mos_pred = model_fc(features)
                loss = emd(dist_gt, dist_pred)
                # loss = criterion(mos_pred, mos_gt)
            
                loss.backward()
                opt_fc.step()
                opt_features.step()
        
                epoch_loss += loss.item()
            
            elapsed_time = time.time() - start_time
            np.savetxt('train_result.txt', np.hstack((epoch+1, epoch_loss/n_count, elapsed_time)), fmt='%2.8f')
            print('epoch = %4d , loss = %4.4f , time = %4.2f s' % (epoch+1, epoch_loss/n_count, elapsed_time))
            if epoch%save_freq==0:
                torch.save(model_features, os.path.join(save_dir_mod, 'model_vgg_%03d.pth' % (epoch+1)))
                torch.save(model_fc, os.path.join(save_dir_mod, 'model_fc_%03d.pth' % (epoch+1)))
        
if __name__ == '__main__':
    Train_nima()