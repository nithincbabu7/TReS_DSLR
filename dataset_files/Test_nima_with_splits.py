
import sys
sys.path.insert(0, '/mnt/e4e2203b-ecf7-4807-b641-1fa71921092d/VIGNESH_benchmarking/maxentropy-master/')

import os
import pandas as pd
import numpy as np
import torch
import csv
from torch.autograd import Variable
# from tqdm import 
from glob import glob
from scipy.stats import spearmanr
from torchvision.transforms.functional import to_tensor  
from torch.utils.data import Dataset, DataLoader
import time
from tqdm import tqdm
import cv2
# from get_labels import get_max_entropy_distribution
# from emd_loss import earth_movers_distance as emd
from model_nima import nima_features, nima_fc


ps = 620

save_freq = 5
cuda1 = torch.device('cuda:0')
load_dir ='/media/ece/Backup Plus/Vignesh/nima_checkpoints/'
img_dir = '/media/ece/Backup Plus/Vignesh/Dataset_in_one_folder/'
outf = './nimapreds/'
os.makedirs(outf,exist_ok=True)
# score_dir = ''

## creating the dataset
class SIDDataset(Dataset):
    def __init__(self, img_dir,MOS,img_names):
       
        self.paths_img = [img_dir + i for i in img_names]
        self.moss = MOS
        
    def __len__(self):
        # return size of dataset
        return len(self.paths_img)
    
    def read_data(self, path):
        
        img = cv2.imread(path)
        img  = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = np.moveaxis(img, (0,1,2), (1,2,0))
        
        img = img.astype(np.float32)
        img = img/255
        
        img[:1,:,:] = (img[0:1,:,:]-0.485)/0.229
        img[1:2,:,:] = (img[1:2,:,:]-0.456)/0.224
        img[2:3,:,:] = (img[2:3,:,:]-0.406)/0.225
        
        return img

    def extract_patches(self, img):
        
        H = img.shape[1]
        W = img.shape[2]
        
        xx = np.random.randint(0,W-ps)
        yy = np.random.randint(0,H-ps)
        patch = img[:,yy:yy+ps,xx:xx+ps]
            
        patch = torch.tensor(patch.copy(), device=cuda1, dtype=torch.float32)
        
        return patch
       

    def __getitem__(self, idx):
       
        path = self.paths_img[idx]
        name_img = os.path.basename(path)
        
        img = self.read_data(path)
        patch = self.extract_patches(img)
        # img = torch.tensor(img.copy(), device=cuda1, dtype=torch.float32)

        df = self.moss
        mos = df['MOS'][df['image_name'] == name_img]
        mos = mos.values[0]
        
        mos = torch.tensor(mos, device=cuda1, dtype=torch.float32)
        mos = mos/10
        
        return patch, mos

mosd = pd.read_csv(img_dir + 'MOS.csv',
                  names=['tp', 'image_name', 'MOS'])
mosd.drop(mosd.columns[0], inplace=True, axis=1)
mosd = mosd.drop([mosd.index[0]])
srocc_runs = []

for split in tqdm(range(1, 101)):


    with open(img_dir + 'testsplits.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for count, row in enumerate(csv_reader):
            if count == split:
                temp = row

    img_names = []

    for name in temp:
        if len(name) > 2:
            img_names.append(name)

    load_dir_mod = load_dir + 'split_' + str(split) + '/'

     
    test_dataloader = DataLoader(SIDDataset(img_dir,mosd,img_names), 
                                  batch_size=1, shuffle=False)
    
    model_features = torch.load(load_dir_mod + 'model_vgg_025.pth')
    model_features = model_features.eval()
    
    model_fc = torch.load(load_dir_mod + 'model_fc_025.pth')
    model_fc = model_fc.eval()
    
    vec = torch.arange(0,10, device = cuda1, dtype=torch.float32)

    for epoch in range(1):

        # step to the learning rate in this epcoh
        epoch_loss = 0
        epoch_regloss = 0
        epoch_reconloss = 0

        start_time = time.time()

        MOS_GT = []
        MOS_EST = []

        for n_count, batch_yx in enumerate(test_dataloader):
            img_data, mos_gt = Variable(batch_yx[0]), Variable(batch_yx[1])

            mos_gt = torch.mean(mos_gt)
        
            with torch.no_grad(): 
           
                features = model_features(img_data)
                dist_pred = model_fc(features)
                # mos_est = model_fc(features)

            mos_est = torch.sum(dist_pred*vec)

            mos_est = Variable(mos_est,
                               requires_grad=False).cpu().numpy()
            mos_gt = Variable(mos_gt,
                              requires_grad=False).cpu().numpy()

            mos_est = np.mean(mos_est)

            MOS_GT.append(mos_gt)
            MOS_EST.append(mos_est)

        MOS_EST = np.asarray(MOS_EST)
        MOS_GT = np.asarray(MOS_GT)
        MOS_EST = MOS_EST[:,np.newaxis]
        MOS_GT = MOS_GT[:,np.newaxis]
        
        rho_test, p_test = spearmanr(MOS_EST, MOS_GT)
        
        # print(rho_test)
        srocc_runs.append(rho_test)
        
        both = np.float32(np.concatenate([MOS_GT,MOS_EST],axis=1))
        np.save(outf + 'split_' + str(split) +'.npy',both,allow_pickle=False)

print(np.median(np.array(srocc_runs)))
print(np.std(np.array(srocc_runs)))
